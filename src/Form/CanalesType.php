<?php

namespace App\Form;

use App\Entity\Canales;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;



class CanalesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre', TextType::class , array(
                'attr' => [
                    'class' => 'form-control',
                ]
            ))
            ->add('descripcion', TextareaType::class , array(
                'attr' => [
                    'class' => 'form-control'
                ]
            ))
            ->add('imagen', FileType::class , array(
                'mapped' => false, 
                'required' => false,
                'attr' => [
                    'widget' => 'single_text',
                    'class' => 'form-control'
                ] 
            ))
            ->add('fechamodificacion', DateType::class , array(
                'widget' => 'single_text',
                'attr' => [
                    'class' => 'form-control'
                ]
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Canales::class,
        ]);
    }
}
