<?php

namespace App\Controller;

use App\Entity\Canales;
use App\Form\CanalesType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/canales")
 */
class CanalesController extends AbstractController
{
    /**
     * @Route("/", name="canales_index", methods={"GET"})
     */
    public function index(): Response
    {
        $canales = $this->getDoctrine()
            ->getRepository(Canales::class)
            ->findAll();

        return $this->render('canales/index.html.twig', [
            'canales' => $canales,
        ]);
    }

    /**
     * @Route("/new", name="canales_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $canale = new Canales();
        $form = $this->createForm(CanalesType::class, $canale);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($canale);
            $entityManager->flush();

            return $this->redirectToRoute('canales_index');
        }

        return $this->render('canales/new.html.twig', [
            'canale' => $canale,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{idCanal}", name="canales_show", methods={"GET"})
     */
    public function show(Canales $canale): Response
    {
        return $this->render('canales/show.html.twig', [
            'canale' => $canale,
        ]);
    }

    /**
     * @Route("/{idCanal}/edit", name="canales_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Canales $canale): Response
    {
        $form = $this->createForm(CanalesType::class, $canale);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $file= $form->get("imagen")->getData();
            if ($file==null){
                } else{
                    $contenido = file_get_contents($file);
                    $canale->setImagen($contenido);
                }

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('canales_index');
        }

        return $this->render('canales/edit.html.twig', [
            'canale' => $canale,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{idCanal}", name="canales_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Canales $canale): Response
    {
        if ($this->isCsrfTokenValid('delete'.$canale->getIdCanal(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($canale);
            $entityManager->flush();
        }

        return $this->redirectToRoute('canales_index');
    }
}
