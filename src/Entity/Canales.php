<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Canales
 *
 * @ORM\Table(name="canales")
 * @ORM\Entity
 */
class Canales
{
    /**
     * @var int
     *
     * @ORM\Column(name="Id_Canal", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idCanal;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Nombre", type="string", length=100, nullable=true)
     */
    private $nombre;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Descripcion", type="text", length=0, nullable=true)
     */
    private $descripcion;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Imagen", type="blob", length=65535, nullable=true)
     */
    private $imagen;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="FechaModificacion", type="datetime", nullable=true)
     */
    private $fechamodificacion;

        /**
     * @ORM\OneToMany(targetEntity="App\Entity\UC", mappedBy="canal", orphanRemoval=true)
     */
    private $ucs;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Conversa", mappedBy="idCanal", orphanRemoval=true)
     */
    private $convus;


    public function getIdCanal(): ?int
    {
        return $this->idCanal;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(?string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(?string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getImagen()
    {
        return $this->imagen;
    }

    public function setImagen($imagen): self
    {
        $this->imagen = $imagen;

        return $this;
    }

    public function getFechamodificacion(): ?\DateTimeInterface
    {
        return $this->fechamodificacion;
    }

    public function setFechamodificacion(?\DateTimeInterface $fechamodificacion): self
    {
        $this->fechamodificacion = $fechamodificacion;

        return $this;
    }

    public function addUc(UC $uc): self
    {
        if (!$this->ucs->contains($uc)) {
            $this->ucs[] = $uc;
            $uc->setCanales($this);
        }

        return $this;
    }


    public function removeUc(UC $uc): self
    {
        if ($this->ucs->contains($uc)) {
            $this->ucs->removeElement($uc);
            // set the owning side to null (unless already changed)
            if ($uc->getCanales() === $this) {
                $uc->setCanales(null);
            }
        }

        return $this;
    }

    public function addconvus(Conversa $conus): self
    {
        if (!$this->convus->contains($conus)) {
            $this->convus[] = $conus;
            $conus->setCanales($this);
        }

        return $this;
    }

    public function removeconvus(Conversa $conus): self
    {
        if ($this->convus->contains($conus)) {
            $this->convus->removeElement($conus);
            // set the owning side to null (unless already changed)
            if ($conus->getCanales() === $this) {
                $conus->setCanales(null);
            }
        }

        return $this;
    }


}
