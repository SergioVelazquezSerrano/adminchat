<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Usuarios
 *
 * @ORM\Table(name="usuarios", uniqueConstraints={@ORM\UniqueConstraint(name="idx_usu_correo", columns={"Correo"})})
 * @ORM\Entity
 */
class Usuarios
{
    /**
     * @var int
     *
     * @ORM\Column(name="Id_Us", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idUs;

    /**
     * @var string
     *
     * @ORM\Column(name="Correo", type="string", length=100, nullable=false)
     */
    private $correo;

    /**
     * @var string
     *
     * @ORM\Column(name="Password", type="string", length=100, nullable=false)
     */
    private $password;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Nombre", type="string", length=100, nullable=true)
     */
    private $nombre;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Apellidos", type="string", length=100, nullable=true)
     */
    private $apellidos;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Puesto", type="string", length=100, nullable=true)
     */
    private $puesto;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Conocimientos", type="text", length=0, nullable=true)
     */
    private $conocimientos;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Aficiones", type="text", length=0, nullable=true)
     */
    private $aficiones;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="Fecha_Nac", type="date", nullable=true)
     */
    private $fechaNac;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="Fecha_Ult_Con", type="datetime", nullable=true)
     */
    private $fechaUltCon;

    /**
     * @var string|null
     *
     * @ORM\Column(name="foto_archivo", type="blob", length=65535, nullable=true)
     */
    private $fotoArchivo;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fechamodificacion", type="datetime", nullable=true)
     */
    private $fechamodificacion;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UC", mappedBy="idUs", orphanRemoval=true)
     */
    private $ucs;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Conversa", mappedBy="idUs", orphanRemoval=true)
     */
    private $convus;


    public function getIdUs(): ?int
    {
        return $this->idUs;
    }

    public function getCorreo(): ?string
    {
        return $this->correo;
    }

    public function setCorreo(string $correo): self
    {
        $this->correo = $correo;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(?string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getApellidos(): ?string
    {
        return $this->apellidos;
    }

    public function setApellidos(?string $apellidos): self
    {
        $this->apellidos = $apellidos;

        return $this;
    }

    public function getPuesto(): ?string
    {
        return $this->puesto;
    }

    public function setPuesto(?string $puesto): self
    {
        $this->puesto = $puesto;

        return $this;
    }

    public function getConocimientos(): ?string
    {
        return $this->conocimientos;
    }

    public function setConocimientos(?string $conocimientos): self
    {
        $this->conocimientos = $conocimientos;

        return $this;
    }

    public function getAficiones(): ?string
    {
        return $this->aficiones;
    }

    public function setAficiones(?string $aficiones): self
    {
        $this->aficiones = $aficiones;

        return $this;
    }

    public function getFechaNac(): ?\DateTimeInterface
    {
        return $this->fechaNac;
    }

    public function setFechaNac(?\DateTimeInterface $fechaNac): self
    {
        $this->fechaNac = $fechaNac;

        return $this;
    }

    public function getFechaUltCon(): ?\DateTimeInterface
    {
        return $this->fechaUltCon;
    }

    public function setFechaUltCon(?\DateTimeInterface $fechaUltCon): self
    {
        $this->fechaUltCon = $fechaUltCon;

        return $this;
    }

    public function getFotoArchivo()
    {
        return $this->fotoArchivo;
    }

    public function setFotoArchivo($fotoArchivo): self
    {
        $this->fotoArchivo = $fotoArchivo;

        return $this;
    }

    public function getFechamodificacion(): ?\DateTimeInterface
    {
        return $this->fechamodificacion;
    }

    public function setFechamodificacion(?\DateTimeInterface $fechamodificacion): self
    {
        $this->fechamodificacion = $fechamodificacion;

        return $this;
    }

    public function addUc(UC $uc): self
    {
        if (!$this->ucs->contains($uc)) {
            $this->ucs[] = $uc;
            $uc->setUsuarios($this);
        }

        return $this;
    }

    public function removeUc(UC $uc): self
    {
        if ($this->ucs->contains($uc)) {
            $this->ucs->removeElement($uc);
            // set the owning side to null (unless already changed)
            if ($uc->getUsuarios() === $this) {
                $uc->setUsuarios(null);
            }
        }

        return $this;
    }

    public function addconvus(Conversa $conus): self
    {
        if (!$this->convus->contains($conus)) {
            $this->convus[] = $conus;
            $conus->setUsuarios($this);
        }

        return $this;
    }

    public function removeconvus(Conversa $conus): self
    {
        if ($this->convus->contains($conus)) {
            $this->convus->removeElement($conus);
            // set the owning side to null (unless already changed)
            if ($conus->getUsuarios() === $this) {
                $conus->setUsuarios(null);
            }
        }

        return $this;
    }



}
