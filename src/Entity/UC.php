<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UC
 *
 * @ORM\Table(name="u_c", uniqueConstraints={@ORM\UniqueConstraint(name="unique", columns={"Id_Us", "Id_Canal"})}, indexes={@ORM\Index(name="ca", columns={"Id_Canal"}), @ORM\Index(name="us", columns={"Id_Us"})})
 * @ORM\Entity
 */
class UC
{
    /**
     * @var int
     *
     * @ORM\Column(name="Id_UC", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idUc;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="Fecha_Inscripcion", type="datetime", nullable=true)
     */
    private $fechaInscripcion;

    /**
     * @var \Canales
     *
     * @ORM\ManyToOne(targetEntity="Canales", inversedBy="cus")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="Id_Canal", referencedColumnName="Id_Canal")
     * })
     */
    private $canal;


    /**
     * @var \Usuarios
     *
     * @ORM\ManyToOne(targetEntity="Usuarios", inversedBy="ucs")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="Id_Us", referencedColumnName="Id_Us")
     * })
     */
    private $idUs;


}
